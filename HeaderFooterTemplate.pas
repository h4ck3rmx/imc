unit HeaderFooterTemplate;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Edit, FMX.Objects;

type
  THeaderFooterForm = class(TForm)
    Header: TToolBar;
    Footer: TToolBar;
    HeaderLabel: TLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    Image1: TImage;
    altura: TEdit;
    peso: TEdit;
    label2: TLabel;
    label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    resultato: TLabel;
    Label5: TLabel;
   procedure calculo;
    procedure alturaChange(Sender: TObject);
    procedure pesoChange(Sender: TObject);
    procedure pesoKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure alturaKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  HeaderFooterForm: THeaderFooterForm;

implementation

{$R *.fmx}




procedure THeaderFooterForm.alturaChange(Sender: TObject);
begin
  calculo;
end;

procedure THeaderFooterForm.alturaKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
calculo;
end;

procedure THeaderFooterForm.calculo;
var
rAltura : Real;
rPeso : Real;
rRest : Real;
sPeso : string;
begin
  if (altura.Text <> '') AND (peso.Text <> '') then
  begin
    // 80/(1.75)^2 = 80/3.0625 = 26.12
    rAltura := StrToFloat(altura.Text);
    rPeso := StrToFloat(peso.Text);
    rRest :=  (rPeso/(rAltura*rAltura));
    resultato.Text :=  FormatFloat('0.00', rRest)+' Kg.';
    if rRest < 18.5 then
    begin
       sPeso := 'Bajo';
    end else if (rRest >= 18.5) AND (rRest <= 24.9)  then
    begin
      sPeso := 'Normal';
    end else if (rRest >= 25) AND (rRest <= 29.9) then
    begin
      sPeso :=  'Sobrepeso';
    end else if (rRest > 30) then
    begin
      sPeso :=  'Obecidad';
    end;
    Label5.Text := sPeso;
  end;
end;

procedure THeaderFooterForm.pesoChange(Sender: TObject);
begin
calculo;
end;

procedure THeaderFooterForm.pesoKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
 calculo;
end;

end.
